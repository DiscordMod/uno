import java.util.Random;
public class DynamicCardArray
{
	private Card[] deck;
	private int pointer;
	
	public DynamicCardArray(int size)
	{
		this.deck = new Card[size];
		this.pointer = 0;
		
		if (size == 92){	
			setupDeck();
		}
	}
		//pointer methods
	public int getPointer()
	{
		return this.pointer;
	}
	public void decreasePointer()
	{
		this.pointer--;
	}

		//card getter method
	public Card getCard(int index)
	{
		return deck[index];
	}
		//hand array methods
		
	public Card removeCardAt(int index)
	{
		Card[] temp = new Card[deck.length-1];
		Card removeCard = deck[index];
		
		
		for(int i = 0; i < index; i++){
			temp[i] = deck[i];
		}
		
		for(int i = index; i < temp.length; i++){
			temp[i] = deck[i+1];
		}
		deck = temp;
		
		return removeCard;
	}
	public Card checkCardAt(int index)
	{
		return deck[index];
	}
	
		//drawing cards
		
	public void addCard(Card c)
	{
		deck[this.pointer] = c;
		pointer++;
		
	}
	
		//changes in the cardpile object
	
	public Card getTopDeck()
	{	
		return deck[0];
	}
	
	public void removeTopDeck()
	{
		Card[] tempArr = new Card[92];
		
		for(int i = 0; i < deck.length - 1; i++){
			tempArr[i] = deck[i + 1];
		}
		deck = tempArr;
		
		pointer--;
	}
			
		//generating deck
	
	public void setupDeck()
	{
		deck();
	}
	
	private void deck()
	{	
		for(Colors c: Colors.values()){
				Card card = new Card(Values.ZERO, c);
				addCard(card);
		}
		for(int i = 0; i < 2; i++){
			for(Colors c1: Colors.values()){
				for(Values v1: Values.values()){
					if(!(v1 == Values.ZERO)){
						Card card = new Card(v1, c1);
						addCard(card);
					}
				}
			}
		}
	}

	public void shuffleDeck()
	{
		Random rand = new Random();

		for(int i = 0; i < this.pointer; i++){
			int randPointer = rand.nextInt(this.pointer);
			
			Card temp = this.deck[i];
            this.deck[i] = this.deck[randPointer];
            this.deck[randPointer] = temp;
		}
	}
	public String toString()
	{
		String list = "";
	
		for(int i = 0; i < this.pointer; i++){	
			list += i + ": " + this.deck[i];
			
			if(i < this.pointer - 1){
				list += " | ";
			}
		}
		return list;
	}
}