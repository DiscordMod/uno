public class Player
{
	private String name;
	private DynamicCardArray hand;
	
	public Player(String name)
	{
		this.name = name;
		this.hand = new DynamicCardArray(30);
	}
	public String getName()
	{
		return this.name;
	}
	public DynamicCardArray getHand()
	{
		return hand;
	}
	
	//this method takes as input an index and removes the card at the index
	//after it is removed, we decrease the pointer because there is one less card in the array
	public Card getRemovedCard(int index)
	{
		Card removeAtIndex = hand.removeCardAt(index);
		
		hand.decreasePointer();
		
		return removeAtIndex;
	}
}
