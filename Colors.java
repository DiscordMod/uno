public enum Colors
{
	RED{
	
		public String toString()
		{
			return red + "Red";
		}
	
	},
	GREEN{
	
		public String toString()
		{
			return green + "Green";
		}
	
	},
	BLUE{
	
		public String toString()
		{
			return blue + "Blue";
		}
	
	},
	YELLOW{
	
		public String toString()
		{
			return bright_yellow + "Yellow";
		}
	
	};
	
	private static final String red = "\033[0;31m";     
    private static final String green = "\033[0;32m";
    private static final String bright_yellow = "\033[0;93m"; 
    private static final String blue = "\033[0;34m";    
	
}

