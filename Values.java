public enum Values
{
	ZERO {
		
		public String toString()
		{
			return "0" + reset;
		}
	},
	ONE{
		public String toString()
		{
			return "1" + reset;
		}
	},
	TWO{
	
		public String toString()
		{
			return "2" + reset;
		}
	
	},
	THREE{
	
		public String toString()
		{
			return "3" + reset;
		}
	
	},
	FOUR{
	
		public String toString()
		{
			return "4" + reset;
		}
	
	},
	FIVE{
	
		public String toString()
		{
			return "5" + reset;
		}
	
	},
	SIX{
	
		public String toString()
		{
			return "6" + reset;
		}
	
	},
	SEVEN{
	
		public String toString()
		{
			return "7" + reset;
		}
	
	},
	EIGHT{
	
		public String toString()
		{
			return "8" + reset;
		}
	
	},
	NINE{
	
		public String toString()
		{
			return "9" + reset;
		}
	
	},
	PLUSTWO{
	
		public String toString()
		{
			return "PlusTwo" + reset;
		}
	
	},
	SKIP{
	
		public String toString()
		{
			return "Skip" + reset;
		}
	
	
	};

	private static final String reset = "\033[0m";
}