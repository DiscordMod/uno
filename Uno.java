import java.util.*;

public class Uno
{
	public static void main(String[] args)
	{	
		Scanner reader = new Scanner(System.in);
		
		DynamicCardArray deck = new DynamicCardArray(92);
		
		//deck
		
		deck.shuffleDeck();
		
		//WELCOME PRINT
		//FLOWER CLASS AND CONSOLE COLORS CLASS ARE NOT PART OF THE PROJECT
		
		Flower welcome = new Flower();

		welcome.printWelcome();
		
		System.out.println("\n");
		System.out.println("\n");
		
		//SET UP PLAYERS
		
		Player[] players = new Player[3];
		
		for(int eachPlayer = 0; eachPlayer < players.length; eachPlayer++){
			System.out.println("Enter the player " + (eachPlayer + 1) + "'s name: ");
			String name = reader.next();
			players[eachPlayer] = new Player(name);
			
			for(int distributeCards = 0; distributeCards < 7; distributeCards++){
				players[eachPlayer].getHand().addCard(deck.getTopDeck());
				deck.removeTopDeck();
			}
		}
		
		//SETTING UP THE ACTUAL GAME
		
		System.out.println("\n");
		
		boolean game = true;
		int currentPlayer = 0;
		
		Card cardPlayed = deck.getTopDeck();
		deck.removeTopDeck();
		
		while(game){
			
			//this print shows the first card of the discard pile, it will later be updated once the turn has ended for the player
			
			System.out.println("The top card is: " + cardPlayed);
			
			System.out.println(players[currentPlayer].getName() + "'s turn: ");
			
			System.out.println(players[currentPlayer].getHand());
			
			//this while loop checks if the user cannot play a card for the cardPlayed
			//this will automatically draw for them
			
			boolean badInput = true;
			
			String input = "";
			
			while(badInput){
				
				boolean ToF = false;
				for(int i = 0; i < players[currentPlayer].getHand().getPointer(); i++){
					if(players[currentPlayer].getHand().getCard(i).getValue().equals(cardPlayed.getValue())){
						ToF = true;
					}
					else if(players[currentPlayer].getHand().getCard(i).getColor().equals(cardPlayed.getColor())){
						ToF = true;
					}
				}

				if(ToF == true){
					System.out.println("Would you like to play or draw a card? Type play or draw");
					
					input = reader.next();
					
					if(input.equals("draw") || input.equals("play")){
						badInput = false;
					}
					else{
						System.out.println("NO! Type play or draw");
					}
				}
				else{
					input = "draw";
					badInput = false;
				}
					
			}
			
				//this if statement checks for the input of draw and play
			
			if(input.equals("play")){
				
				boolean notValidTurn = true;
				int card = 0;
				
				System.out.println("Which card will you place? From 0 to " + (players[currentPlayer].getHand().getPointer() - 1));
				
				while(notValidTurn){
					
					try{
						
						card = reader.nextInt();
						
					//tester print:	System.out.println(players[currentPlayer].getHand().getCard(card));
						
						boolean InvalidCardNumber = true;
					
					//the if is checking for cards that are of the same color or of the same value
					//and isn't going past the pointer or under 0
					
						while(InvalidCardNumber){
							if(card <= players[currentPlayer].getHand().getPointer() - 1 && card >= 0 && (players[currentPlayer].getHand().checkCardAt(card).getColor().equals(cardPlayed.getColor()) || players[currentPlayer].getHand().checkCardAt(card).getValue().equals(cardPlayed.getValue()))){
								cardPlayed = players[currentPlayer].getHand().getCard(card);
								InvalidCardNumber = false;
								notValidTurn = false;
							}else{
								System.out.println("This card cannot be played!");
								System.out.println("Please input another card number: ");
								
								card = reader.nextInt();
							}
						}
						//when inputting a number for the index of the card, it will catch if you enter anything other than an int
						
					}catch (InputMismatchException e){
						System.out.println("That's not an integer!");
						System.out.println("Please input another card number: ");
						reader.next();
					}
					
				}
				
				//this if statement checks whether the user inputting a special card
				
				if(players[currentPlayer].getHand().checkCardAt(card).getValue().equals(Values.PLUSTWO)){
					
					//this nested if statement checks if the last player placed a plus two because
					//by skipping their turn, we'd have to increase the currentPlayer by one
					
					if(currentPlayer >= players.length - 1){
						
						int distributeCardTo = 0;

						players[distributeCardTo].getHand().addCard(deck.getTopDeck());
						deck.removeTopDeck();
						
						players[distributeCardTo].getHand().addCard(deck.getTopDeck());
						deck.removeTopDeck();
						
					}else{
						
						players[currentPlayer+1].getHand().addCard(deck.getTopDeck());
						deck.removeTopDeck();
						
						players[currentPlayer+1].getHand().addCard(deck.getTopDeck());
						deck.removeTopDeck();
							
					}
					
				}else if(players[currentPlayer].getHand().checkCardAt(card).getValue().equals(Values.SKIP)){
					
					if(currentPlayer >= players.length){
						currentPlayer = 0;
					}
				
				}
				
				//updates the card played
				
				cardPlayed = players[currentPlayer].getRemovedCard(card);
				
				//we update the card played first and then add one to the current player
				
				if(cardPlayed.getValue().equals(Values.SKIP) || cardPlayed.getValue().equals(Values.PLUSTWO)){
					currentPlayer++;
				}

				if(currentPlayer >= players.length){
					currentPlayer = 0;
				}
				
			
			}else if(input.equals("draw")){
					
				players[currentPlayer].getHand().addCard(deck.getTopDeck());
				deck.removeTopDeck();
			}
			
			//if the pointer of player is equal to 0, the player wins
			
			if(players[currentPlayer].getHand().getPointer() == 0){
				System.out.println(players[currentPlayer].getName() + " won, YOU'RE SO GOOD AT THE GAME!!!!");
				game = false;
			}
			
			//if the deck runs out of cards, we generate a new deck and shuffle it
			
			if(deck.getPointer() == 0){
				deck = new DynamicCardArray(92);
				deck.shuffleDeck();
			}
			
			//next turn
			
			currentPlayer++;
			
			//if it's the last person that has played, we need to set the current player to 0 to not have an array index out of bounds
			
			if(currentPlayer >= players.length){
				currentPlayer = 0;
			}
			
			System.out.println("\n");
		}
		 
		System.out.println("THANK YOU FOR PLAYING!");
	}
}