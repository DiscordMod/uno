public class Cardpile
{ 
	private DynamicCardArray deck; 
	
	public Cardpile()
	{
		this.deck = new DynamicCardArray(); 
		deck();
	}
	
	public void add(Card c){
		deck.add(Card c);
	}
	
	private void deck()
	{	
		for(Colors c: Colors.values()){
				Card card = new Card(Values.ZERO, c);
				deck.addCard(card);
		}
		for(int i = 0; i < 2; i++){
			for(Colors c1: Colors.values()){
				for(Values v1: Values.values()){
					if(!(v1 == Values.ZERO)){
						Card card = new Card(v1, c1);
						deck.addCard(card);
					}
				}
			}
		}
	}
	public String toString()
	{
		String list = "";
	
		for(int i = 0; i < this.deck.getLength(); i++){	
			list += this.deck.getDeck()[i];
			
			if(i < this.deck.getLength() - 1){
				list += ", ";
			}
		}
		return list;
	}
	public DynamicCardArray getDeck()
	{
		return deck;
		
	}
}
